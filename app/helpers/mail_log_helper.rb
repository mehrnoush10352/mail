module MailLogHelper

    def generateHtmlForAjax(mailLogs)
        result = ''
        @data = mailLogs
        if @data 
            @data.each do |mailLog|
                result = result + "
                <div class='row mailLog-item'>
                    <a href='/show/#{mailLog.id}'>
                        <div class='col-xs-2'>
                            #{mailLog.subject}
                        </div>
                        <div class='col-xs-4'>
                            #{mailLog.sender}
                        </div>
                        <div class='col-xs-4'>
                            #{mailLog.mail_date}
                        </div>
                    </a>
                </div> "   
            end
        end
        result
    end
end
