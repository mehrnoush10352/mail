class EmailAddress < ApplicationRecord
  validates_uniqueness_of :local_part, :scope => :domain_id

  belongs_to :domain

  def self.create_random(domain_id = 1)
    o = [('a'..'z'), ('A'..'Z')].map(&:to_a).flatten
    string = (0...10).map { o[rand(o.length)] }.join

    self::create!(domain_id: domain_id, local_part: string)

    rescue ActiveRecord::RecordInvalid
      retry
  end

  def address
    self.local_part + '@' + self.domain.name
  end

  def messages
    Message.where(message_to: self.address)
  end
end
