
class MailLogController < ApplicationController

  def show
    @mailLog = MailLog.find(params[:id])
  end

  def index
    if session[:email]
      @randomEmail = session[:email]  
    else
      o = [('a'..'z'), ('A'..'Z')].map(&:to_a).flatten
      string = (0...10).map { o[rand(o.length)] }.join
      @randomEmail = string + '@clockwise.ir'
      session[:email] = @randomEmail
    end

    @mailLogs = MailLog.where(to: "#{@randomEmail}")
  end

  def update
    emailAddress = params[:email]
    email =  MailLog.where(to: emailAddress).last
    if email
      if email.created_at < 1.minute.ago
        @mailLogs = MailLog.where(to: email)
        #respond_with
        render :update
      else
        false
      end
    end
  end
end

