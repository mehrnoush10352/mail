class AjaxController < ApplicationController
    include MailLogHelper 

    def updateEmails
        emailAddress = params[:email]
        email =  MailLog.where(to: emailAddress).last
        respond_to do |format|
            # if the response fomat is html, redirect as usua
            if email
                if email.created_at < Time.now - 1.minute
                    @mailLogs = MailLog.where("to = ? and created_at > ?", emailAddress,  Time.now - 1.minute)
                    if @mailLogs
                        template = generateHtmlForAjax(@mailLogs)

                    end
                    format.json { render json: [
                        'error' => true,
                        'result' => template

                    ] }       

                else
                    format.json { render json: [
                        'error' => true,
                        'message' => 'no new mail'

                    ] }    
                end
            else
                format.json { render json: [
                    'error' => true,
                    'message' => 'email not found'
                ] }
            end
        end
    end
end

