class MessageController < ApplicationController
  
  def inbox
    @messages = Message.where(message_to: @email.address)
  end

  def show
    @message = Message.find(params[:id])
    raise 'access denied' unless @message.message_to == @email.address
  end

  def newcomming
    @messages = @email.messages

    if params[:last]
      @messages = @messages.where('id > ?', params[:last])
    end

    render json: @messages
  end

  def cable
    message = Message.last
    ActionCable.server.broadcast 'messages', message
    head :ok
  end
end
