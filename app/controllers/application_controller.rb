class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception

  before_action :set_email_address
  before_action :set_locale

  def set_email_address
    if session['email']
      @email  = EmailAddress.find_by(local_part: session['email']['local_part'], domain_id: session['email']['domain_id'])
    else
      @email  = EmailAddress.create_random
      session['email'] = {
        local_part: @email.local_part,
        domain_id:  @email.domain_id
      }
    end
  end

  def set_locale
    I18n.locale = :fa
  end
end
