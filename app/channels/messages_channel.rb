class MessagesChannel < ApplicationCable::Channel  
  def subscribed
    stream_from current_user
  end

  def current_user
    "session_#{session.id}"
  end
end
