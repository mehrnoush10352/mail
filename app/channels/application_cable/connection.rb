module ApplicationCable
  class Connection < ActionCable::Connection::Base
=begin
    identified_by :current_user

    def connect
      self.current_user = find_verified_user
    end

    protected
    def find_verified_user
      if current_user = EmailAddress.find_by(domain_id: session['email']['domain_id'], local_part: session['email']['local_part'])
        current_user
      else
        reject_unauthorized_connection
      end
    end
=end    
  end
end
