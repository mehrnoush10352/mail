// app/assets/javascripts/channels/chatrooms.js

//= require cable
//= require_self
//= require_tree .

this.App = {};

App.cable = ActionCable.createConsumer();


App.messages = App.cable.subscriptions.create('MessagesChannel', {  
  received: function(data) {
    $('.inbox-messages').append(this.renderMessage(data));
  },

  renderMessage: function(data) {

    return  "<div class='row message'>" + 
                "<a href='/show/" + data.id + "'>" +
                    "<div class='col-xs-2'>" +
                        data.message_subject +
                    "</div>" +
                    "<div class='col-xs-4'>" +
                      data.sender_email +
                    "</div>" +
                    "<div class='col-xs-4'>" +
                        data.message_date +
                    "</div>" +
                "</a>"+
              "</div> ";
  }
});
