class CreateMailLogs < ActiveRecord::Migration[5.1]
  def change
    create_table :mail_logs do |t|
      t.string :sender
      t.string :receiver
      t.text :body
      t.string :to
      t.datetime :email_date
      t.datetime :log_date

      t.timestamps
    end
  end
end
