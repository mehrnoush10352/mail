class DropMailLogs < ActiveRecord::Migration[5.1]
  def change
    drop_table :mail_logs
  end
end
