class CreateMessages < ActiveRecord::Migration[5.1]
  def change
    create_table :messages do |t|
      t.string 'sender_email'
      t.string 'receiver_email'
      t.string 'message_to'
      t.string 'message_subject'
      t.text   'message_body'
      t.datetime 'message_date'
   
      t.timestamps
    end
  end
end
