class FixEmailAddressSpell < ActiveRecord::Migration[5.1]
  def change
    rename_table :email_adresses, :email_addresses
  end
end
