class AddLocalPartToEmailAddress < ActiveRecord::Migration[5.1]
  def change
    add_column :email_addresses, :local_part, :string
  end
end
