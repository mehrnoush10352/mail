class AddDomainToEmailAddresses < ActiveRecord::Migration[5.1]
  def change
    add_reference :email_addresses, :domain, foreign_key: true
  end
end
