class RemoveLogDateFromMailLogs < ActiveRecord::Migration[5.1]
  def change
    remove_column :mail_logs, :log_date
  end
end
