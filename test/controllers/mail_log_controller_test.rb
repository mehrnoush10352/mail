require 'test_helper'

class MailLogControllerTest < ActionDispatch::IntegrationTest
  test "should get show" do
    get mail_log_show_url
    assert_response :success
  end

  test "should get index" do
    get mail_log_index_url
    assert_response :success
  end

end
