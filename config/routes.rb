Rails.application.routes.draw do
  mount ActionCable.server => '/cable'
  get 'test' => 'message#cable'

  get '/message/newcomming'  => 'message#newcomming'
  get '/message/:id' => 'message#show', as: :message_show
  get '/mail/update' => 'ajax#updateEmails',  defaults: {format: 'json'}
  root 'message#inbox'
end
